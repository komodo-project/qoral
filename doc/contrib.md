# Contributions

Contributing is welcome and allowed! We thank you for wanting to improve Qoral. However, please note the branching scheme:

- The **stable** branch contains proper versions. Do not push to it without a merge request!
- If you want to commit, create a branch named **beta-<CHANGE_NAME>**.