# Qoral Editor 🪸

The Qoral Editor is a rewriting of the legacy Monoxide editor in C++.
I used C++ since I declare it to be much better and efficient than C.
It has the same features of Monoxide so far, with more being in development.
Qoral employs a philosophy of minimalism and can be functionally compared to something like Notepad.
That being said, I'm planning to include syntax highlighting and some useful keybinds. Nothing spectacular like Neovim - we got projects for some complex things as well.

The point is, that Qoral will not let you down.
No modal keys, no complex setup, just run and enjoy.