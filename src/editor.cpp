#include <iostream>
#include <fstream>
#include <cstdlib>

std::string req() {
    std::string filename;
    std::cout << "[QORAL] INPUT FILE NAME: ";
    std::getline(std::cin, filename);

    std::string directory;
    std::cout << "[QORAL] INPUT DIRECTORY (leave blank for default): ";
    std::getline(std::cin, directory);

    // If directory is left blank, use the default directory
    if (directory.empty()) {
        const char* home = getenv("HOME");
        if (home) {
            directory = std::string(home) + "/";
        } else {
            std::cerr << "[QORAL] ERROR: HOME environment variable not set.\n";
            exit(1);
        }
    }

    // Append filename to directory
    std::string filepath = directory + filename;

    return filepath;
}

std::string read(std::string file_body) {
    std::cout << "[QORAL] INSERT FILE CONTENT (Ctrl+D TWICE TO SAVE):\n\n";
    std::ofstream file(file_body);
    if (file.is_open()) {
        std::string line;
        while (std::getline(std::cin, line)) {
            if (std::cin.eof()) { // check if Ctrl+D was pressed
                break;
            }
            file << line << "\n";
        }
        file.close();
    } else { // error handling
        std::cout << "[QORAL] UNABLE TO OPEN FILE [" << file_body << "]";
    }
    return file_body;
}