#include <iostream>
#include <fstream>
#include <cstdlib>
//! nepoužívať OFSTREAM, použiť FSTREAM
#include "editor.cpp"

int main(int argc, char *argv[]) {
    std::string filename;
    std::string file_body;
    std::string file_contents;
    filename = req();
    file_body = filename; 
    read(file_body);

    std::ifstream file(file_body);
    if (file.is_open()) {
        std::string line;
        while (std::getline(file, line)) {
            file_contents += line + "\n";
        }
        file.close();
    } else {
        std::cout << "[QORAL] UNABLE TO OPEN FILE [" << filename << "]";
    }

    std::cout << file_contents << std::endl;
    return 0;
}